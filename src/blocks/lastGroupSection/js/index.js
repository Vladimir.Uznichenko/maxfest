$(document).ready(function () {
    if ($(".js_lastGroupSection__list").length > 0) {
        $(".js_lastGroupSection__list").owlCarousel({
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                450: {
                    items: 2,
                    nav: false
                },
                800: {
                    items: 3,
                    nav: true,
                    loop: false
                }
            }
        });
    }
});