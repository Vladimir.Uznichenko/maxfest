$(function () {
    //Смена цвета иконки поиска при наведении
    $('.js_slideSearchField__text').hover(
        function () {
            $('.js_slideSearchField__img').attr('src', 'images/b_slideSearchField__img_yellow.png');
        },
        function () {
            $('.js_slideSearchField__img').attr('src', 'images/b_slideSearchField__img.png');
        }
    );
});
$(document).on('click', '.js_slideSearchField__text', function () {
    $('.b_slideSearchField__text').css({
        "display":"none"
    });
    $('.b_slideSearchField__input').css({
            "display":"block",
            "opacity":"1",
            "width":"240px",
            "position":"relative"
        }
    );
    $('.b_slideSearchField__inputHiddenClose').css({
            "opacity":"0.7",
            "top": "-43px",
            "right": "10px"
        }
    );
});
$(document).on('click', '.js_slideSearchField__inputHiddenClose', function () {
    $('.b_slideSearchField__text').css({
        "display":"inline-block"
    });
    $('.b_slideSearchField__input').css({
            "opacity":"0",
            "width":"240px",
            "position":"relative",
            "display":"none"
        }
    );
    $('.b_slideSearchField__inputHiddenClose').css({
            "opacity":"0",
            "top": "-43px",
            "right": "10px"
        }
    );
});