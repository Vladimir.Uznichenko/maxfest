//repeat for production build
$(document).on('click', '.js_button__festShowMore', function () {
    $.ajax({
        url: '../ajaxhtml/festShowMore.html',
        success: function (data) {

            $('html, body').animate({
                scrollTop: '+=300px'
            }, 500);
            $('.js_festSection__list').append(data);
            //alert('Load was performed.');
        }
    });
});

$(document).on('click', '.js_button__groupShowMore', function () {
    $.ajax({
        url: '../ajaxhtml/groupShowMore.html',
        success: function (data) {
            $('html, body').animate({
                scrollTop: '+=300px'
            }, 500);
            $('.js_groupSection__list').append(data);
            //alert('Load was performed.');
        }
    });
});

$(document).on('click', '.js_button__galleryShowMore', function () {
    $.ajax({
        url: '../ajaxhtml/galleryShowMore.html',
        success: function (data) {
            $('html, body').animate({
                scrollTop: '+=300px'
            }, 500);
            $('.b_gallerySection__list .row').append(data);
            //alert('Load was performed.');
        }
    });
});

$(document).on('click', '.js_button__reviewShowMore', function () {
    $.ajax({
        url: '../ajaxhtml/reviewShowMore.html',
        success: function (data) {
            $('html, body').animate({
                scrollTop: '+=300px'
            }, 500);
            $('.js_reviewsSection__list').append(data);
            //alert('Load was performed.');
        }
    });
});