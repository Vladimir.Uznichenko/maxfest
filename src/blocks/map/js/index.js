var customLabel = {
    fest: {
        label: 'F'
    }
};
var styles = [[{
    url: '../images/pin.png',
    height: 48,
    width: 30,
    anchor: [-18, 0],
    textColor: '#ffffff',
    textSize: 10,
    iconAnchor: [15, 48]
}]];
var options = {
    styles: styles[0],
    imagePath: '../images/m'
};
if ($('#map').length > 0) {
    $(document).ready(function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: new google.maps.LatLng(-33.863276, 151.207977),
                zoom: 12
            });
            var infoWindow = new google.maps.InfoWindow;

            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                map.setCenter(pos);
            });
            // Change this depending on the name of your PHP or XML file
            downloadUrl('../ajaxHtml/map__pointers.xml', function (data) {
                var xml = data.responseXML;
                var markerss = [];
                var markers = xml.documentElement.getElementsByTagName('marker');
                Array.prototype.forEach.call(markers, function (markerElem) {
                    var name = markerElem.getAttribute('name');
                    var address = markerElem.getAttribute('address');
                    var image = markerElem.getAttribute('image');
                    var type = markerElem.getAttribute('type');
                    var link = markerElem.getAttribute('link');
                    var point = new google.maps.LatLng(
                        parseFloat(markerElem.getAttribute('lat')),
                        parseFloat(markerElem.getAttribute('lng')));

                    var infowincontent = document.createElement('div');
                    infowincontent.setAttribute("class", "b_mapPointer");
                    var photo = document.createElement('div');
                    photo.setAttribute("class", "b_mapPointer__image");

                    var imgLink = document.createElement('a');
                    imgLink.setAttribute("class", "b_mapPointer__link");
                    imgLink.setAttribute("href", link);
                    photo.appendChild(imgLink);

                    var photoImg = document.createElement('img');
                    photoImg.src = '../images/' + image;
                    infowincontent.appendChild(photo);
                    imgLink.appendChild(photoImg);

                    var content = document.createElement('div');
                    content.setAttribute("class", "b_mapPointer__content");
                    infowincontent.appendChild(content);

                    var header = document.createElement('div');
                    header.setAttribute("class", "b_mapPointer__header");
                    header.textContent = name;
                    content.appendChild(header);

                    var place = document.createElement('div');
                    place.setAttribute("class", "b_mapPointer__place");
                    place.textContent = address;
                    content.appendChild(place);

                    var button = document.createElement('div');
                    button.setAttribute("class", "b_mapPointer__button");
                    content.appendChild(button);

                    var buttonLink = document.createElement('a');
                    buttonLink.setAttribute("class", "b_mapPointer__link");
                    buttonLink.setAttribute("href", link);
                    button.appendChild(buttonLink);

                    var arrow = document.createElement('div');
                    arrow.setAttribute("class", "b_mapPointer__arrow");
                    buttonLink.appendChild(arrow);

                    var arrowImg = document.createElement('img');
                    arrowImg.setAttribute("src", "../images/b_mapPointer__arrow.png");
                    arrow.appendChild(arrowImg);

                    var buttonMore = document.createElement('div');
                    buttonMore.setAttribute("class", "b_mapPointer__more");
                    buttonMore.textContent = "Подробнее";
                    buttonLink.appendChild(buttonMore);

                    var icon = customLabel[type] || {};
                    var marker = new google.maps.Marker({
                        map: map,
                        position: point,
                        label: icon.label
                    });

                    markerss.push(marker);

                    marker.addListener('click', function () {
                        infoWindow.setContent(infowincontent);
                        infoWindow.open(map, marker);
                    });
                });
                var markerCluster = new MarkerClusterer(map, markerss, options);
            });
        }
    );
}

function downloadUrl(url, callback) {
    var request = window.ActiveXObject ?
        new ActiveXObject('Microsoft.XMLHTTP') :
        new XMLHttpRequest;

    request.onreadystatechange = function () {
        if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
        }
    };
    request.open('GET', url, true);
    request.send(null);
}
function doNothing() {
}