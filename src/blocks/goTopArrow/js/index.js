$(document).scroll(function () {
    if ($(this).scrollTop() > 500) {
        $('.js_goTopArrow').css({
            'bottom': '250px'
        });
    }
    if ($(this).scrollTop() < 500) {
        $('.js_goTopArrow').css({
            'bottom': '-50px'
        });
    }
});

$(document).on('click', '.js_goTopArrow', function () {
    $("html,body").animate({scrollTop: 0}, "slow");
    return false;
});

