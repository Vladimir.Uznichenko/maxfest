$(document).scroll(function () {
    if ($(this).scrollTop() > 1) {
        $('.b_page__header').addClass('b_page__header_sticky');
        $('.b_page__header').removeClass('b_page__header_transparent');
    }
    if ($(this).scrollTop() < 1) {
        $('.b_page__header').addClass('b_page__header_transparent');
        $('.b_page__header').removeClass('b_page__header_sticky');
    }
});