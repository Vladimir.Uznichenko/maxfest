$(document).ready(function () {
    $(".b_groupTopSection__menuItem:first-child a").addClass("b_groupTopSection__menuLink_active");
    $(".b_groupTopSection__menuItem").on("click", "a", function (event) {
        $(".b_groupTopSection__menuItem a").removeClass("b_groupTopSection__menuLink_active"); //удаляем класс во всех вкладках
        $(this).addClass("b_groupTopSection__menuLink_active"); //добавляем класс текущей (нажатой)
        //отменяем стандартную обработку нажатия по ссылке
        event.preventDefault();
        //забираем идентификатор бока с атрибута href
        var id = $(this).attr('href'),
            //узнаем высоту от начала страницы до блока на который ссылается якорь
            top = $(id).offset().top;
        //анимируем переход на расстояние - top за 700 мс
        $('body,html').animate({scrollTop: top}, 900);
    });
});