$(document).on('click', '.js_searchForm__searchFilterCursorPointer_open', function () {
    $('.js_searchForm__searchFilter').css({"display":"none"});
    $('.js_searchForm_searchFilterToggleP').html('Расширенный поиск');
    $('.js_searchForm__searchFilterToggleImg').removeClass('b_searchForm__searchFilterToggleImg_reverse');
    $('.js_searchForm__searchFilterCursorPointer_open').addClass('js_searchForm__searchFilterCursorPointer');
    $('.js_searchForm__searchFilterCursorPointer_open').removeClass('js_searchForm__searchFilterCursorPointer_open');
});

$(document).on('click', '.js_searchForm__searchFilterCursorPointer', function () {
    $('.js_searchForm__searchFilter').css({"display":"flex"});
    $('.js_searchForm_searchFilterToggleP').html('Скрыть');
    $('.js_searchForm__searchFilterToggleImg').addClass('b_searchForm__searchFilterToggleImg_reverse');
    $('.js_searchForm__searchFilterCursorPointer').addClass('js_searchForm__searchFilterCursorPointer_open');
    $('.js_searchForm__searchFilterCursorPointer').removeClass('js_searchForm__searchFilterCursorPointer');
});
