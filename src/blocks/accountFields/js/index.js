$('#js_accountFields__input_years').datepicker({
    // Можно выбрать тольо даты, идущие за сегодняшним днем, включая сегодня
    maxDate: new Date(),
    minDate:new Date('1900'),
    autoClose:true
});
$('#js_accountFields__input_months').datepicker({
    autoClose:true
});
$('#js_accountFields__input_days').datepicker({
    autoClose:true
});

